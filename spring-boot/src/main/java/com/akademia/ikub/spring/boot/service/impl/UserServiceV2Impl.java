package com.akademia.ikub.spring.boot.service.impl;

import com.akademia.ikub.spring.boot.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Service
@Qualifier("UserServiceV2Impl")
@ConditionalOnBean(name = "userServiceV1Impl")
public class UserServiceV2Impl implements UserService {
    @Override
    public String getInformations() {
        return "hello from user service V2";
    }

    @Override
    public String getType() {
        return "type1";
    }
}
