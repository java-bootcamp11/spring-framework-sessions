package com.akademia.ikub.spring.boot.service.impl;

import com.akademia.ikub.spring.boot.service.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("UserServiceV1Impl")
public class UserServiceV1Impl implements UserService {

    @Override
    public String getInformations() {
        return "hello from user service V1";
    }

    @Override
    public String getType() {
        return "type";
    }
}
