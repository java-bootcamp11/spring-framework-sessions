package com.akademia.ikub.spring.boot.scheduling;

public interface ScheduleService {

    void fixedRate();
    void initialDelay();
    void cronScheduling();
}
