package com.akademia.ikub.spring.boot.service.impl;

import com.akademia.ikub.spring.boot.service.AnyService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

@ConditionalOnProperty(prefix = "com.anyBean",name = "enabled",havingValue = "true")
@Service
public class AnyServiceImpl implements AnyService {
    @Override
    public void doSomthing() {
        System.err.println("I am doing something here");
    }
}
