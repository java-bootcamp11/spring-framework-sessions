package com.akademia.ikub.spring.boot;

import com.akademia.ikub.spring.boot.service.AnyService;
import com.akademia.ikub.spring.boot.service.UserService;
import com.akademia.ikub.spring.boot.service.configuration.UserConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.Map;


@EnableScheduling
@EnableConfigurationProperties
@SpringBootApplication
public class Application implements CommandLineRunner {

    @Autowired
    @Qualifier("UserServiceV1Impl")
    private UserService userServiceV1;

    @Autowired
    @Qualifier("UserServiceV2Impl")
    private UserService getUserServiceV2;

    @Autowired
    private Map<String,UserService> userServiceMap;

    @Autowired
    private UserConfig userConfig;

    @Value("${user.name1}")
    private String name;
    @Value("${user.surname}")
    private String surname;

    @Autowired(required = false)
    private AnyService anyService;


    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        System.err.println(userServiceV1.getInformations());
        System.err.println(getUserServiceV2.getInformations());
        String type = "type1";



      UserService u =   userServiceMap.entrySet().stream()
                .filter(b -> b.getValue().getType().equals(type))
                .findFirst()
                .orElse(null).getValue();

        System.err.println(u.getInformations());

        System.err.println(userConfig);

        System.err.println(name);
        System.err.println(surname);

//        anyService.doSomthing();
    }
}
