package com.akademia.ikub.spring.boot.service;

public interface UserService {

    String getInformations();

    String getType();
}
