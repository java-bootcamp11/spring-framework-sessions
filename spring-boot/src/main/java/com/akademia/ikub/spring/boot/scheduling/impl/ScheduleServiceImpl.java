package com.akademia.ikub.spring.boot.scheduling.impl;

import com.akademia.ikub.spring.boot.scheduling.ScheduleService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Scheduled(fixedRate = 5000)
    @Override
    public void fixedRate() {
        System.err.println(String.format("fixedRate %s",LocalDateTime.now()));

    }

    @Scheduled(fixedRate = 5000,initialDelay = 10000)
    @Override
    public void initialDelay() {
        System.err.println(String.format("initialDelay %s",LocalDateTime.now()));
    }

    @Scheduled(cron = "*/10 * * * * *")
    @Override
    public void cronScheduling() {
        System.err.println(String.format("Cron Scheduling %s", LocalTime.now()));
    }


}
