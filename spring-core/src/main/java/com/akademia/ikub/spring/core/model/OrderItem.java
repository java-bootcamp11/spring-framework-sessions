package com.akademia.ikub.spring.core.model;

public class OrderItem {
  private Long id;
  private Integer quantity;
  private Order order;
  private Product product;

  public OrderItem() {
  }

  public OrderItem(Long id, Integer quantity, Order order, Product product) {
    this.id = id;
    this.quantity = quantity;
    this.order = order;
    this.product = product;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }
}