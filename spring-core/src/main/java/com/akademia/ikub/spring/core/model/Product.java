package com.akademia.ikub.spring.core.model;

public class Product {
  private Long id;
  private String name;
  private String description;
  private Integer price;
  private Integer inStock;

  public Product() {
  }

  public Product(Long id, String name, String description, Integer price, Integer inStock) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.inStock = inStock;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Integer getPrice() {
    return price;
  }

  public void setPrice(Integer price) {
    this.price = price;
  }

  public Integer getInStock() {
    return inStock;
  }

  public void setInStock(Integer inStock) {
    this.inStock = inStock;
  }

  @Override
  public String toString() {
    return "Product{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", description='" + description + '\'' +
            ", price=" + price +
            ", inStock=" + inStock +
            '}';
  }
}