package com.akademia.ikub.spring.core;

import com.akademia.ikub.spring.core.model.User;
import com.akademia.ikub.spring.core.model.enums.Role;
import com.akademia.ikub.spring.core.repository.NotificationGatewayRepository;
import com.akademia.ikub.spring.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class SpringCoreApplication {

    private UserRepository userRepository;
    private NotificationGatewayRepository notificationGatewayRepository;

    @Autowired
    public SpringCoreApplication(UserRepository userRepository, NotificationGatewayRepository notificationGatewayRepository) {
        this.userRepository = userRepository;
        this.notificationGatewayRepository = notificationGatewayRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringCoreApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx){
        return args -> {
            System.out.println("Get user with ID "+1);
            User u = userRepository.getUser(1L);
            System.err.println(u);
            System.out.println("=====================");

            System.out.println("Create user");
            User user = new User(4L,"user4@email.com","","user4", Role.ADMIN);
            userRepository.createUser(user);
            System.err.println(userRepository.getUser(4L));
            System.out.println("=====================");

            System.out.println("Update user");
            User userUpdateResponse = new User(4L,"user4@yahoo.com","","user4", Role.CUSTOMER);
            userRepository.updateUser(4L,userUpdateResponse);
            System.err.println(userRepository.getUser(4L));
            System.out.println("=====================");

            System.out.println("Update user");
            userRepository.deleteUser(4L);
            System.err.println(userRepository.getUser(4L));
            System.out.println("=====================");


        };
    }


    public void printBeans(ApplicationContext ctx){
        List<String> beanNames = Arrays.stream(ctx.getBeanDefinitionNames())
                .collect(Collectors.toList());
        beanNames.forEach(b ->{
            System.out.println(b);
        });
    }

}
