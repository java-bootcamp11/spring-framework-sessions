package com.akademia.ikub.spring.core.model.enums;

import java.util.Arrays;

public enum NotificationType {
  EMAIL("EMAIL"),
  SMS("SMS");

  private String value;

  NotificationType(String type){
    this.value = type;
  }

  public static NotificationType fromValue(String type){
    return Arrays.stream(NotificationType.values())
            .filter(n -> n.getValue().equals(type))
            .findFirst()
            .orElse(null);
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "NotificationType{" +
            "value='" + value + '\'' +
            '}';
  }
}