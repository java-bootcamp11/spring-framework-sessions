package com.akademia.ikub.spring.core.model;

import java.time.LocalDateTime;

public class Order {
  private Long orderId;
  private LocalDateTime orderDate;
  private User user;

  public Order() {
  }

  public Order(Long orderId, LocalDateTime orderDate, User user) {
    this.orderId = orderId;
    this.orderDate = orderDate;
    this.user = user;
  }

  public Long getOrderId() {
    return orderId;
  }

  public void setOrderId(Long orderId) {
    this.orderId = orderId;
  }

  public LocalDateTime getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(LocalDateTime orderDate) {
    this.orderDate = orderDate;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "Order{" +
            "orderId=" + orderId +
            ", orderDate=" + orderDate +
            ", user=" + user +
            '}';
  }
}