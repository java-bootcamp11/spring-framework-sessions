package com.akademia.ikub.spring.core.repository;


import com.akademia.ikub.spring.core.model.Notification;

public interface NotificationGatewayRepository {

    Notification getNotification(Long id);
    Notification createNotification(Notification notification);
    Notification updateNotification(Long id,Notification notification);
    Notification deleteNotification(Long id);
}
