package com.akademia.ikub.spring.core.model.enums;

import java.util.Arrays;

public enum Role {
    CUSTOMER("CUSTOMER"),
    ADMIN("ADMIN");

    private String value;

    Role(String role){
        this.value = role;
    }

    public static Role fromValue(String role){
        return Arrays.stream(Role.values())
                .filter(r-> role.equals(role))
                .findFirst()
                .orElse(null);
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Role{" +
                "value='" + value + '\'' +
                '}';
    }
}
