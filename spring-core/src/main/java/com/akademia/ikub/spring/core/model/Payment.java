package com.akademia.ikub.spring.core.model;

import com.akademia.ikub.spring.core.model.enums.PaymentStatus;

public class Payment {
  private Long id;
  private Order order;
  private String transactionId;
  private PaymentStatus status;

  public Payment() {
  }

  public Payment(Long id, Order order, String transactionId, PaymentStatus status) {
    this.id = id;
    this.order = order;
    this.transactionId = transactionId;
    this.status = status;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public String getTransactionId() {
    return transactionId;
  }

  public void setTransactionId(String transactionId) {
    this.transactionId = transactionId;
  }

  public PaymentStatus getStatus() {
    return status;
  }

  public void setStatus(PaymentStatus status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "Payment{" +
            "id=" + id +
            ", order=" + order +
            ", transactionId='" + transactionId + '\'' +
            ", status=" + status +
            '}';
  }
}