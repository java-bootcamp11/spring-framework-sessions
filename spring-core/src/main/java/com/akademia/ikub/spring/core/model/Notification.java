package com.akademia.ikub.spring.core.model;

import com.akademia.ikub.spring.core.model.enums.NotificationType;

public class Notification {
  private Long id;
  private User user;
  private NotificationType type;
  private String message;

  public Notification() {
  }

  public Notification(Long id, User user, NotificationType type, String message) {
    this.id = id;
    this.user = user;
    this.type = type;
    this.message = message;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public NotificationType getType() {
    return type;
  }

  public void setType(NotificationType type) {
    this.type = type;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return "Notification{" +
            "id=" + id +
            ", user=" + user +
            ", type=" + type +
            ", message='" + message + '\'' +
            '}';
  }
}