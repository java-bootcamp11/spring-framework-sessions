package com.akademia.ikub.spring.core.model.enums;

import java.util.Arrays;

public enum PaymentStatus {
  SUCCESS("SUCCESS"),
  FAILURE("FAILURE");

  private String value;

  PaymentStatus(String status){
    this.value = status;
  }

  private static PaymentStatus fromValue(String status){
    return Arrays.stream(PaymentStatus.values())
            .filter(p-> p.getValue().equals(status))
            .findFirst()
            .orElse(null);
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "PaymentStatus{" +
            "value='" + value + '\'' +
            '}';
  }
}