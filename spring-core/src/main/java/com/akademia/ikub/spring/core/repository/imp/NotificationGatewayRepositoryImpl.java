package com.akademia.ikub.spring.core.repository.imp;

import com.akademia.ikub.spring.core.model.Notification;
import com.akademia.ikub.spring.core.model.enums.NotificationType;
import com.akademia.ikub.spring.core.repository.NotificationGatewayRepository;
import com.akademia.ikub.spring.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Repository
public class NotificationGatewayRepositoryImpl implements NotificationGatewayRepository {

    private UserRepository userRepository;

    @Autowired
    public NotificationGatewayRepositoryImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    private Map<Long,Notification> notifications;

    @PostConstruct
    public void init(){
        notifications = new HashMap<>();
        notifications.put(1L,new Notification(1L,userRepository.getUser(1l), NotificationType.EMAIL,"message desc"));
        notifications.put(1L,new Notification(2L,userRepository.getUser(2l), NotificationType.EMAIL,"message desc"));
    }

    @Override
    public Notification getNotification(Long id) {
        return notifications.getOrDefault(id,null);
    }

    @Override
    public Notification createNotification(Notification notification) {
        return notifications.put(notification.getId(),notification);
    }

    @Override
    public Notification updateNotification(Long id, Notification notification) {
        Notification n = notifications.getOrDefault(id,null);
        if(n!=null){
            notifications.remove(id);
            notifications.put(id,notification);
            return  notifications.get(id);
        }
        return null;
    }

    @Override
    public Notification deleteNotification(Long id) {
        return notifications.remove(id);
    }
}
