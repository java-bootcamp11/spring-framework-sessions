package com.akademia.ikub.spring.core.repository;

import com.akademia.ikub.spring.core.model.User;

public interface UserRepository {

    User getUser(Long id);
    User createUser(User user);
    User updateUser(Long id,User user);
    User deleteUser(Long id);

}
