package com.akademia.ikub.spring.core.repository.imp;

import com.akademia.ikub.spring.core.model.User;
import com.akademia.ikub.spring.core.model.enums.Role;
import com.akademia.ikub.spring.core.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private Map<Long,User> users;

    @PostConstruct
    public void init(){
        users = new HashMap<>();
        users.put(1L,new User(1L,"user1@email.com","","user1",Role.CUSTOMER));
        users.put(2L,new User(2L,"user2@email.com","","user2",Role.CUSTOMER));
        users.put(3L,new User(3L,"user3@email.com","","user3",Role.ADMIN));
    }

    @Override
    public User getUser(Long id) {
        return users.getOrDefault(id,null);
    }

    @Override
    public User createUser(User user) {
        return users.put(user.getId(),user);
    }

    @Override
    public User updateUser(Long id, User user) {
        User u = users.get(id);
        if(u!=null){
            users.remove(user.getId());
            users.put(user.getId(),user);
            return users.get(user.getId());
        }
        return null;
    }

    @Override
    public User deleteUser(Long id) {
        return users.remove(id);
    }

    public Map<Long, User> getUsers() {
        return users;
    }

    public void setUsers(Map<Long, User> users) {
        this.users = users;
    }
}
