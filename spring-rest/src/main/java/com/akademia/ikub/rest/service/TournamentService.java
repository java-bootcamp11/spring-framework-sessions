package com.akademia.ikub.rest.service;

import com.akademia.ikub.rest.dto.TournamentDTO;

public interface TournamentService {

    TournamentDTO addTournament(TournamentDTO t);
    TournamentDTO addCategory(Integer id, Integer categoryId);
}
