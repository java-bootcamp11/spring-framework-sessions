package com.akademia.ikub.rest.dto.mapper;

import com.akademia.ikub.jpa.entity.Player;
import com.akademia.ikub.jpa.entity.PlayerProfile;
import com.akademia.ikub.rest.dto.PlayerDTO;

public class PlayerMapper {

    public static PlayerDTO toDto(Player p){
        return PlayerDTO.builder()
                .id(p.getId())
                .name(p.getName())
                .profileId(p.getProfile()!=null?p.getProfile().getId():null)
                .profile(p.getProfile()!=null?p.getProfile().getTwitter():null)
                .build();
    }

    public static Player toEntity(PlayerDTO p){
        return Player.builder()
                .id(p.getId())
                .name(p.getName())
                .profile(PlayerProfile.builder()
                        .id(p.getProfileId())
                        .twitter(p.getProfile())
                        .build())
                .build();

    }
}
