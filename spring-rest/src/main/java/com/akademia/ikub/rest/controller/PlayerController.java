package com.akademia.ikub.rest.controller;

import com.akademia.ikub.rest.dto.PlayerDTO;
import com.akademia.ikub.rest.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/players")
public class PlayerController {
//    @Autowired
//    private PlayerService playerService;
    private final PlayerService playerService;

    @GetMapping("/{id}")
    public ResponseEntity<PlayerDTO> getPlayerById(@PathVariable Integer id){
        return ResponseEntity.ok(playerService.getPlayerById(id));
    }

    @GetMapping
    public ResponseEntity<List<PlayerDTO>> getPlayers(){
        return ResponseEntity.ok(playerService.getPlayers());
    }

    @PostMapping
    public ResponseEntity<PlayerDTO> addPlayer(@RequestBody PlayerDTO playerDTO){
        return ResponseEntity.ok(playerService.addPlayer(playerDTO));
    }

    @PutMapping("/{id}")
    public ResponseEntity<PlayerDTO> updatePlayer(@PathVariable Integer id,
                                                  @RequestBody PlayerDTO playerDTO){
        return ResponseEntity.ok(playerService.updatePlayer(id,playerDTO));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePlayerById(@PathVariable Integer id){
        playerService.deletePlayer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }




}
