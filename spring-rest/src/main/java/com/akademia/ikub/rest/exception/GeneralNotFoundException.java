package com.akademia.ikub.rest.exception;

public class GeneralNotFoundException extends RuntimeException{

    public GeneralNotFoundException(String message){
        super(message);
    }
}
