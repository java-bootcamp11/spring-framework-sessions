package com.akademia.ikub.rest.service.impl;

import com.akademia.ikub.jpa.entity.Player;
import com.akademia.ikub.jpa.repository.PlayerRepository;
import com.akademia.ikub.rest.dto.PlayerDTO;
import com.akademia.ikub.rest.dto.mapper.PlayerMapper;
import com.akademia.ikub.rest.exception.GeneralNotFoundException;
import com.akademia.ikub.rest.service.PlayerService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PlayerServiceImpl implements PlayerService {

//    @Autowired
//    private PlayerRepository playerRepository;
    private final PlayerRepository playerRepository;

    @Override
    public List<PlayerDTO> getPlayers() {
        return playerRepository.findAll()
                .stream()
                .map(p -> PlayerMapper.toDto(p))
                .collect(Collectors.toList());
    }

    @Override
    public PlayerDTO getPlayerById(Integer id) {
        return playerRepository.findById(id)
                .map(p -> PlayerMapper.toDto(p))
                .orElseThrow(()-> new GeneralNotFoundException(String
                        .format("player with id %s not found",id)));
    }

    @Override
    public PlayerDTO addPlayer(PlayerDTO playerDTO) {
       Player player = playerRepository.save(PlayerMapper.toEntity(playerDTO));
       return PlayerMapper.toDto(player);
    }

    @Override
    public PlayerDTO updatePlayer(Integer id, PlayerDTO playerDto) {
        playerDto.setId(id);
        Player pl =  playerRepository.findById(id)
                .map(p -> playerRepository.save(PlayerMapper.toEntity(playerDto)))
                .orElseThrow(()-> new GeneralNotFoundException(String
                        .format("player with id %s not found",id)));
        return PlayerMapper.toDto(pl);

    }

    @Override
    public void deletePlayer(Integer id) {
        PlayerDTO playerDTO = getPlayerById(id);
        playerRepository.deleteById(id);
    }
}
