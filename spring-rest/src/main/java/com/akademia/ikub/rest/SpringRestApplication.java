package com.akademia.ikub.rest;

import com.akademia.ikub.jpa.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@ComponentScan(basePackages = "com.akademia.ikub")
@SpringBootApplication
public class SpringRestApplication{

    @Autowired
    PlayerRepository repository;

    public static void main(String[] args) {
        SpringApplication.run(SpringRestApplication.class, args);
    }
}
