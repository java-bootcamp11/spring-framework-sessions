package com.akademia.ikub.rest.controller.exceptionHandling;

import com.akademia.ikub.rest.dto.exception.GeneralResourceErrorResponse;
import com.akademia.ikub.rest.exception.GeneralNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@ControllerAdvice
public class GenericExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<GeneralResourceErrorResponse> handlePlayerNotFountException(
            GeneralNotFoundException ex, HttpServletRequest request){

        GeneralResourceErrorResponse resp = GeneralResourceErrorResponse.builder()
                .timestamp(LocalDateTime.now())
                .statusCode(HttpStatus.NOT_FOUND)
                .path(request.getRequestURI())
                .message(ex.getMessage())
                .build();
        return new ResponseEntity(resp,HttpStatus.NOT_FOUND);
    }


}
