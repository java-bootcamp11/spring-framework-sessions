package com.akademia.ikub.rest.service.impl;

import com.akademia.ikub.jpa.entity.Category;
import com.akademia.ikub.jpa.entity.Tournament;
import com.akademia.ikub.jpa.repository.CategoryRepository;
import com.akademia.ikub.jpa.repository.TournamentRepository;
import com.akademia.ikub.rest.dto.TournamentDTO;
import com.akademia.ikub.rest.dto.mapper.TournamentMapper;
import com.akademia.ikub.rest.exception.GeneralNotFoundException;
import com.akademia.ikub.rest.service.TournamentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class TournamentServiceImpl implements TournamentService {

    private final TournamentRepository tournamentRepository;
    private final CategoryRepository categoryRepository;

    @Override
    public TournamentDTO addTournament(TournamentDTO t) {
        return TournamentMapper.toDto(tournamentRepository
                .save(TournamentMapper.toEntity(t)));
    }

    @Override
    public TournamentDTO addCategory(Integer id, Integer categoryId) {
        Tournament t = tournamentRepository.findById(id)
                .orElseThrow(() -> new GeneralNotFoundException(String
                        .format("Tournament with id %s not found",id)));
        Category c = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new GeneralNotFoundException(String
                        .format("Category with id %s not found",categoryId)));
        t.getCategories().add(c);

        return TournamentMapper.toDto(tournamentRepository.save(t));
    }
}
