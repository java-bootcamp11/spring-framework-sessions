package com.akademia.ikub.rest.dto.mapper;

import com.akademia.ikub.jpa.entity.Category;
import com.akademia.ikub.jpa.entity.Tournament;
import com.akademia.ikub.rest.dto.TournamentDTO;

import java.util.List;
import java.util.stream.Collectors;

public class TournamentMapper {

    public static TournamentDTO toDto(Tournament t) {
        List<String> categories = null;
        if (t.getCategories() != null) {
            categories =  t.getCategories()
                    .stream()
                    .map(c -> c.getName())
                    .collect(Collectors.toList());
        }
        return TournamentDTO.builder()
                .id(t.getId())
                .name(t.getName())
                .location(t.getLocation())
                .categories(categories)
                .build();
    }

    public static Tournament toEntity(TournamentDTO t) {
        System.err.println(t.getName());
        System.err.println(t.getLocation());
        return Tournament.builder()
                .id(t.getId())
                .name(t.getName())
                .location(t.getLocation())
                .build();

    }

}