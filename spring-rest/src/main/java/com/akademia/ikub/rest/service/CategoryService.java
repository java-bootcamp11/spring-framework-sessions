package com.akademia.ikub.rest.service;

import com.akademia.ikub.rest.dto.CategoryDTO;

import java.util.List;

public interface CategoryService {

    CategoryDTO addCategory(CategoryDTO c);
    List<CategoryDTO> getCategories();
}
