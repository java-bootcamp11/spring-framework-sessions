package com.akademia.ikub.rest.service;

import com.akademia.ikub.jpa.entity.Player;
import com.akademia.ikub.rest.dto.PlayerDTO;

import java.util.List;

public interface PlayerService {

    List<PlayerDTO> getPlayers();
    PlayerDTO getPlayerById(Integer id);
    PlayerDTO addPlayer(PlayerDTO playerDTO);
    PlayerDTO updatePlayer(Integer id, PlayerDTO playerDto);
    void deletePlayer(Integer id);
}
