package com.akademia.ikub.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TournamentDTO {
    private Integer id;
    private String name;
    private String location;
    private List<String> categories;
}
