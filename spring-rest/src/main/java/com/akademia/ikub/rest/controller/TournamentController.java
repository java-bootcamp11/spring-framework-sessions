package com.akademia.ikub.rest.controller;

import com.akademia.ikub.rest.dto.TournamentDTO;
import com.akademia.ikub.rest.dto.mapper.TournamentMapper;
import com.akademia.ikub.rest.service.TournamentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/tournaments")
public class TournamentController {

    private final TournamentService tournamentService;

    @PostMapping
    public TournamentDTO addTournament(@RequestBody TournamentDTO t){
        return tournamentService.addTournament(t);
    }

    @GetMapping("/{tournamentId}/category/{categoryId}")
    public TournamentDTO addTournamentCategory(@PathVariable Integer tournamentId,
                                               @PathVariable Integer categoryId){
        return tournamentService.addCategory(tournamentId,categoryId);
    }
}
