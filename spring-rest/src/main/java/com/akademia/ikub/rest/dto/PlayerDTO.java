package com.akademia.ikub.rest.dto;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PlayerDTO {
    private Integer id;
    private String name;
    private Integer profileId;
    private String profile;
}
