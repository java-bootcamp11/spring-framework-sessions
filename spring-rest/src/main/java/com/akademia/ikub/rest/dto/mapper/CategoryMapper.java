package com.akademia.ikub.rest.dto.mapper;

import com.akademia.ikub.jpa.entity.Category;
import com.akademia.ikub.rest.dto.CategoryDTO;
import com.akademia.ikub.rest.dto.TournamentDTO;

import java.util.List;
import java.util.stream.Collectors;

public class CategoryMapper {

    public static Category toEntity(CategoryDTO c){
        return Category.builder()
                .id(c.getId())
                .name(c.getName())
                .build();
    }
    public static CategoryDTO toDto(Category e){

        List<TournamentDTO> tournaments = null;
        if (e.getTournaments()!=null){
            tournaments = e.getTournaments().stream()
                    .map(t -> TournamentMapper.toDto(t))
                    .collect(Collectors.toList());
        }

        return CategoryDTO.builder()
                .id(e.getId())
                .name(e.getName())
                .tournaments(tournaments)
                .build();
    }
}
