package com.akademia.ikub.jpa.repository;

import com.akademia.ikub.jpa.entity.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PlayersSpringRepository extends JpaRepository<Player,Integer>{

    List<Player> findAllByNameAndNationality(String name, String nationality);

}
