package com.akademia.ikub.jpa.repository;

import com.akademia.ikub.jpa.entity.Player;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class PlayerRepository {

    @PersistenceContext
    private EntityManager em;

    public Player insertPlayer(Player player){
        return em.merge(player);
    }

    public Player updatePlayer(Player player){
        return em.merge(player);
    }

    public Player findPlayerById(Integer id) throws RuntimeException{
        return em.find(Player.class,id);
    }

    public void deletePlayerById(Integer id) throws Exception{
        Player toBeDeleted = this.findPlayerById(id);
        em.remove(toBeDeleted);
    }

    public List<Player> getAllPlayes(){
        return em.createNamedQuery("get_all_players",Player.class).getResultList();
    }

    public List<Player> getTopPlayer(Integer titles){
        return em.createNamedQuery("get_all_top_player",Player.class)
                .setParameter("titles",titles).getResultList();
    }

    public List<Player> getPlayerByNationalityAndTitles(String nationality, Integer titles){
        return em.createNamedQuery("get_all_top_players_by_nationality_and_titles",Player.class)
                .setParameter("nationality",nationality)
                .setParameter("titles",titles)
                .getResultList();
    }


}
