package com.akademia.ikub.jpa.entity;

import javax.persistence.*;
import java.time.LocalDate;

@NamedQueries(
        {
                @NamedQuery(name = "get_all_players", query = "select p from Player  p"),
                @NamedQuery(name = "get_all_top_player", query = "select p from Player p where p.titles > :titles"),
                @NamedQuery(name = "get_all_top_players_by_nationality_and_titles",
                        query = "select p from Player p where p.nationality= :nationality and p.titles > :titles")
        }
)
@Entity
@Table(name = "player")
public class Player {
    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "name")
    private String name;
    @Column(name = "nationality")
    private String nationality;
    @Column(name = "birthDate")
    private LocalDate birthDate;
    @Column(name = "titles")
    private Integer titles;

    public Player() {
    }

    public Player(String name, String nationality, LocalDate birthDate, Integer titles) {
        this.name = name;
        this.nationality = nationality;
        this.birthDate = birthDate;
        this.titles = titles;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Integer getTitles() {
        return titles;
    }

    public void setTitles(Integer titles) {
        this.titles = titles;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nationality='" + nationality + '\'' +
                ", birthDate=" + birthDate +
                ", titles=" + titles +
                '}';
    }
}
