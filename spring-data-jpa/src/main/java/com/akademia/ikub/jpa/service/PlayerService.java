package com.akademia.ikub.jpa.service;

import com.akademia.ikub.jpa.entity.Player;
import com.akademia.ikub.jpa.repository.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PlayerService {

    @Autowired
    private PlayerRepository playerRepository;

    @Transactional
    public Player insertPlayer(Player player){
        return playerRepository.insertPlayer(player);
    }

    @Transactional
    public Player updatePlayer(Player player){
        return playerRepository.updatePlayer(player);
    }

    public Player findPlayerById(Integer id){
        try {
            return playerRepository.findPlayerById(id);
        }catch (RuntimeException e){
            System.err.println("Player does not exist "+e.getLocalizedMessage());
        }
        return null;
    }

    @Transactional
    public void deletePlayerById(Integer id){
        try {
            playerRepository.deletePlayerById(id);
        }catch (Exception e){
            throw new RuntimeException(e.getLocalizedMessage());
        }

    }

    public List<Player> getAllPlayes(){
        return playerRepository.getAllPlayes();
    }

    public List<Player> getTopPlayer(Integer titles){
        return playerRepository.getTopPlayer(titles);
    }

    public List<Player> getPlayerByNationalityAndTitles(String nationality, Integer titles){
        return playerRepository.getPlayerByNationalityAndTitles(nationality,titles);
    }




}
