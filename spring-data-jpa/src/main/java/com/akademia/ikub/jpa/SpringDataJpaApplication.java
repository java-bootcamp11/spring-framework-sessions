package com.akademia.ikub.jpa;

import com.akademia.ikub.jpa.entity.Player;
import com.akademia.ikub.jpa.repository.PlayersSpringRepository;
import com.akademia.ikub.jpa.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
public class SpringDataJpaApplication implements CommandLineRunner {

    @Autowired
    private PlayerService playerService;

    @Autowired
    private PlayersSpringRepository playersSpringRepository;

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        Player p = new Player("Player 1","Albania", LocalDate.parse("1990-08-22"),5);
        Player p1 = new Player("Player 2","Kosovo", LocalDate.parse("1990-08-22"),2);
        Player p2 = new Player("Player 3","China", LocalDate.parse("1990-08-22"),6);
        Player p3 = new Player("Player 4","Italy", LocalDate.parse("1990-08-22"),7);
        Player p4 = new Player("Player 5","Albania", LocalDate.parse("1990-08-22"),8);
        Player p5 = new Player("Player 6","Albania", LocalDate.parse("1990-08-22"),1);



        Player optionalPlayer = playersSpringRepository.findById(1)
                .orElseThrow(()-> new RuntimeException("customer not found"));

        playersSpringRepository.deleteById(1);






    }
}
