package com.akademia.ikub.mvc.thymelaf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FirstMvcThymeleafProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(FirstMvcThymeleafProjectApplication.class, args);
    }

}
