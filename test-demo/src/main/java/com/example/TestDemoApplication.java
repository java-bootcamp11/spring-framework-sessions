package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@SpringBootApplication
public class TestDemoApplication {

    public static void main(String[] args) {
        var data1 = new DivisionRolesData(1,"divisinonName1","path1",1,"positionName1",1);
        var data2 = new DivisionRolesData(2,"divisinonName2","path2",2,"positionName2",2);
        List<DivisionRolesData> dList = Arrays.asList(data1,data2);

        System.err.println(toDTO2(dList));

        SpringApplication.run(TestDemoApplication.class, args);
    }

    public static List< DivisionRoles > toDTO2(List <DivisionRolesData> divisionRolesData) {
        Map<Integer,Map<String,Map<String,List<DivisionRolesData>>>> data = divisionRolesData.stream().collect(
                Collectors.groupingBy(DivisionRolesData::getDivisionId,
                        Collectors.groupingBy(DivisionRolesData::getDivisionName,
                                Collectors.groupingBy(DivisionRolesData::getPath))));

        return getFrontendData(data);
    }


    public static List<DivisionRoles> getFrontendData(Map<Integer,Map<String,Map<String,List <DivisionRolesData>>>> data){
      return   data.entrySet().stream()
                .map(d1->{
                    Integer divisionId = d1.getKey();
                    return d1.getValue().entrySet().stream()
                            .map( d2 -> {
                                String divisionName = d2.getKey();
                                return d2.getValue().entrySet().stream()
                                        .map(d3->{
                                            String path = d3.getKey();
                                            List<PositionModel> positions = d3.getValue()
                                                    .stream()
                                                    .map(val -> new PositionModel(val.getPositionId(),val.getPositionName(),val.getCount()))
                                                    .collect(Collectors.toList());
                                            return new DivisionRoles(divisionId,divisionName,path,positions);
                                        }).collect(Collectors.toList());
                            }).flatMap(d -> d.stream()).collect(Collectors.toList());
                })
              .flatMap(toReturn -> toReturn.stream())
              .collect(Collectors.toList());

    }


}
