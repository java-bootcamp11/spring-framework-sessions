package com.example;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DivisionRolesData {

  private Integer divisionId;
  private String divisionName;
  private String path;
  private Integer positionId;
  private String positionName;
  private long count;
}