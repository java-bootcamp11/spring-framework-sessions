package com.example;

import lombok.*;

import java.util.List;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DivisionRoles {
  private Integer divisionId;
  private String divisionName;
  private String path;
  private List < PositionModel > positions;
}