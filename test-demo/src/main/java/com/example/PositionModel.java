package com.example;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PositionModel {
  private Integer id;
  private String name;
  private long count;
}