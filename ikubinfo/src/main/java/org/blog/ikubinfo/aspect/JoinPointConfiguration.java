package org.blog.ikubinfo.aspect;

import org.aspectj.lang.annotation.Pointcut;

public class JoinPointConfiguration {

    @Pointcut(value = "execution(* org.blog.ikubinfo.controller.*.*(..))")
    public void controllerLogging(){}

    @Pointcut(value = "execution(* org.blog.ikubinfo.service.*.*(..))")
    public void serviceControllerLogging(){}

    @Pointcut(value = "org.blog.ikubinfo.aspect.JoinPointConfiguration.controllerLogging() ||" +
            "org.blog.ikubinfo.aspect.JoinPointConfiguration.serviceControllerLogging()")
    public void serviceControllerLoging(){}
}
