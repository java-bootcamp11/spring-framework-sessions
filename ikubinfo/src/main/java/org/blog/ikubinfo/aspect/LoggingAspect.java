package org.blog.ikubinfo.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDateTime;

@Configuration
@Aspect
public class LoggingAspect {

    Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Before("bean(categoryServiceImpl)")
    public void logMethodAccess(JoinPoint joinPoint){
        logger.info("Bean Method {} got triggered",joinPoint.getSignature().getName());
    }

//    @Before("execution(* org.blog.ikubinfo.controller.PostController.*(..))")
//    public void logMethodAccessPost(JoinPoint joinPoint){
//        logger.info("Post Controller Method {} got triggered",joinPoint.getSignature().getName());
//    }
//
//    @AfterReturning("execution(* org.blog.ikubinfo.controller.*.*(..))")
//    public void logAfterReturn(JoinPoint joinPoint){
//        logger.info("After Returning Method {} got triggered",joinPoint.getSignature().getName());
//    }
//
//    @AfterThrowing(value = "execution(* org.blog.ikubinfo.controller.*.*(..))",throwing = "exception")
//    public void logAfterThrowing(JoinPoint joinPoint,Object exception){
//        logger.info("After Throwing Method {} got triggered ERROR : {}"
//                ,joinPoint.getSignature().getName(),exception.toString());
//    }


    @Around(value = "@annotation(org.blog.ikubinfo.aspect.MeasureTime)")
    public Object aroundAspect(ProceedingJoinPoint join) throws Throwable {
        Long startTime =  System.currentTimeMillis();
        Object o = join.proceed();
        Long endTime =  System.currentTimeMillis();
        Long finalTime = endTime - startTime;
        logger.info("Method {} took {} mills to execute",
                join.getSignature().getName(),finalTime);
        return o;
    }
}
