INSERT INTO customers (first_name, last_name,email,phone, password, address, zipcode) VALUES
('John', 'Doe', 'johndoe@example.com','1234567890', 'password1', '123 Main St', '12345'),
('Jane', 'Doe', 'janedoe@example.com','1234567890', 'password2', '456 Main St', '67890'),
('Jim', 'Smith', 'jimsmith@example.com','1234567890', 'password3', '789 Main St', '11111'),
('Jill', 'Smith', 'jillsmith@example.com','1234567890', 'password4', '246 Main St', '22222'),
('Bob', 'Johnson', 'bobjohnson@example.com','1234567890', 'password5', '369 Main St', '33333');

INSERT INTO products (name, description, price, image) VALUES
('Product 1', 'Description of product 1', 100.00, 'product1.jpg'),
('Product 2', 'Description of product 2', 200.00, 'product2.jpg'),
('Product 3', 'Description of product 3', 300.00, 'product3.jpg'),
('Product 4', 'Description of product 4', 400.00, 'product4.jpg'),
('Product 5', 'Description of product 5', 500.00, 'product5.jpg');

INSERT INTO orders (customer_id, total_amount, order_date) VALUES
(1, 100.00, '2023-02-12 10:00:00'),
(2, 200.00, '2023-02-12 11:00:00'),
(3, 300.00, '2023-02-12 12:00:00'),
(4, 400.00, '2023-02-12 13:00:00'),
(5, 500.00, '2023-02-12 14:00:00'),
(1, 100.00, '2023-02-12 10:00:00'),
(2, 200.00, '2023-02-12 11:00:00'),
(3, 300.00, '2023-02-12 12:00:00'),
(4, 400.00, '2023-02-12 13:00:00'),
(5, 500.00, '2023-02-12 14:00:00');


INSERT INTO order_items (order_id, product_id, quantity) VALUES
(1, 1, 2),
(1, 2, 3),
(2, 3, 4),
(3, 4, 5),
(4, 5, 6),
(5, 1, 2),
(6, 2, 3),
(7, 3, 4),
(8, 4, 5),
(9, 4, 5),
(10, 5, 6),
(1, 1, 2),
(1, 2, 3),
(2, 3, 4),
(3, 4, 5),
(4, 5, 6),
(5, 1, 2),
(6, 2, 3),
(7, 3, 4),
(8, 4, 5),
(9, 4, 5),
(10, 5, 6);
