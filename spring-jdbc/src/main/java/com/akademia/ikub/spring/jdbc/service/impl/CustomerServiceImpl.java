package com.akademia.ikub.spring.jdbc.service.impl;

import com.akademia.ikub.spring.jdbc.dao.CustomerDAO;
import com.akademia.ikub.spring.jdbc.dao.OrderDAO;
import com.akademia.ikub.spring.jdbc.model.Customer;
import com.akademia.ikub.spring.jdbc.model.Order;
import com.akademia.ikub.spring.jdbc.service.CustomerService;
import com.akademia.ikub.spring.jdbc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDAO customerDAO;

    @Autowired
    private OrderService orderService;

    @Override
    public List<Customer> getCustomers() {
        return customerDAO.getCustomers();
    }

    @Override
    public Customer getCustomerById(Long id) {
        Customer c = customerDAO.getCustomerById(id);
        c.setOrders(orderService.getOrdersByCustomerId(c.getId()));
        return c;
    }

    @Override
    public Boolean createCustomer(Customer customer) {
        return customerDAO.createCustomer(customer);
    }

    @Override
    public Boolean updateCustomer(Long id, Customer customer) {
        return customerDAO.updateCustomer(id,customer);
    }

    @Override
    public Boolean deleteCustomer(Long id) {
        return customerDAO.deleteCustomer(id);
    }
}
