package com.akademia.ikub.spring.jdbc;

import com.akademia.ikub.spring.jdbc.model.Customer;
import com.akademia.ikub.spring.jdbc.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@SpringBootApplication
public class SpringJdbcApplication {

    @Autowired
    private CustomerService customerService;

    public static void main(String[] args) {
        SpringApplication.run(SpringJdbcApplication.class, args);
    }

}
