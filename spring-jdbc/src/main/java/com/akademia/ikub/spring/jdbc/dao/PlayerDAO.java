package com.akademia.ikub.spring.jdbc.dao;

import com.akademia.ikub.spring.jdbc.model.Player;

import java.util.List;

public interface PlayerDAO {
    List<Player> getAllPlayers();
    Player getPlayerById(Integer id);
    Boolean createPlayer(Player player);
    Boolean updatePlayer(Integer id, Player player);
    Boolean deletePlayer(Integer id);
}
