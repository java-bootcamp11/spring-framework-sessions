package com.akademia.ikub.spring.jdbc.model;

public class OrderItem {
  private Long id;

  private Order order;

  private Product product;

  private Integer quantity;

  public OrderItem() {
  }

  public OrderItem(Long id, Order order, Product product, Integer quantity) {
    this.id = id;
    this.order = order;
    this.product = product;
    this.quantity = quantity;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Order getOrder() {
    return order;
  }

  public void setOrder(Order order) {
    this.order = order;
  }

  public Product getProduct() {
    return product;
  }

  public void setProduct(Product product) {
    this.product = product;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  @Override
  public String toString() {
    return "OrderItem{" +
            "id=" + id +
            ", order=" + order +
            ", product=" + product +
            ", quantity=" + quantity +
            '}';
  }
}