package com.akademia.ikub.spring.jdbc.dao;

import com.akademia.ikub.spring.jdbc.model.Customer;

import java.util.List;

public interface CustomerDAO {

    List<Customer> getCustomers();
    Customer getCustomerById(Long id);
    Boolean createCustomer(Customer customer);
    Boolean updateCustomer(Long id, Customer customer);
    Boolean deleteCustomer(Long id);

}
