package com.akademia.ikub.spring.jdbc.dao.impl;

import com.akademia.ikub.spring.jdbc.dao.OrderItemsDAO;
import com.akademia.ikub.spring.jdbc.model.OrderItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderItemsDAOImpl implements OrderItemsDAO {

    private static final String GET_ITEMS_BY_ORDER_ID = "SELECT * FROM ORDER_ITEMS WHERE order_id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<OrderItem> getOrderItemsByOrderId(Long orderId) {
        return jdbcTemplate.query(GET_ITEMS_BY_ORDER_ID,new BeanPropertyRowMapper<>(OrderItem.class)
            ,new Object[]{orderId});
    }
}
