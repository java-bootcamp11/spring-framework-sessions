package com.akademia.ikub.spring.jdbc.dao;

import com.akademia.ikub.spring.jdbc.model.Order;

import java.util.List;

public interface OrderDAO {

    List<Order> getOrdersByCustomerId(Long customerId);
    Order getOrderById(Long orderId);
}
