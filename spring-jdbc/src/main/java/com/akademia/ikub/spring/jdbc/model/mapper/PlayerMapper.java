package com.akademia.ikub.spring.jdbc.model.mapper;

import com.akademia.ikub.spring.jdbc.model.Player;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerMapper implements RowMapper<Player> {
    @Override
    public Player mapRow(ResultSet rs, int rowNum) throws SQLException {
        Player player = new Player();
        player.setId(rs.getInt("id"));
        player.setName(rs.getString("name"));
        player.setNationality(rs.getString("nationality"));
        player.setBirthDate(rs.getTime("birth_date"));
        player.setTitles(rs.getInt("titles"));
        return player;
    }
}
