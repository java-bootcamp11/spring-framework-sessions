package com.akademia.ikub.spring.jdbc.dao.impl;

import com.akademia.ikub.spring.jdbc.dao.PlayerDAO;
import com.akademia.ikub.spring.jdbc.model.Player;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
@Repository
public class PlayerDAOImpl implements PlayerDAO {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final String GET_ALL_PLAYERS_QUERY = "SELECT * FROM PLAYER";

    @Override
    public List<Player> getAllPlayers() {
        return jdbcTemplate.query(GET_ALL_PLAYERS_QUERY
                ,new BeanPropertyRowMapper<Player>(Player.class));
    }

    private static final String GET_PLAYER_BY_ID_QUERY = "SELECT * FROM PLAYER WHERE ID = ?";

    @Override
    public Player getPlayerById(Integer id) {
        return jdbcTemplate.queryForObject(GET_PLAYER_BY_ID_QUERY,
                new BeanPropertyRowMapper<>(Player.class),
                new Object[] {id});
    }

    private static final String CREATE_PLAYER = "INSERT INTO PLAYER (ID, Name, Nationality,Birth_date, Titles) " +
            "VALUES (?, ?, ?, ?, ?)";

    @Override
    public Boolean createPlayer(Player player) {
        int create = jdbcTemplate.update( CREATE_PLAYER, new Object[]
                { player.getId(), player.getName(), player.getNationality(),
                        new Timestamp(player.getBirthDate().getTime()),
                        player.getTitles()
                });
        return create==-1?false:true;
    }

    private static final String UPDATE_PLAYER = "UPDATE PLAYER SET Name = ?, Nationality = ?, Birth_date = ? " +
            ", Titles = ? WHERE ID = ?";
    @Override
    public Boolean updatePlayer(Integer id, Player player) {

        int update = jdbcTemplate.update( UPDATE_PLAYER, new Object[] {
                player.getName(),
                player.getNationality(),
                new Timestamp(player.getBirthDate().getTime()),
                player.getTitles(),
                id }
        );
        return update==-1?false:true;
    }

    private static final String DELETE_PLAYER = "DELETE FROM PLAYER WHERE ID = ?";

    @Override
    public Boolean deletePlayer(Integer id) {
        int delete=  jdbcTemplate.update(DELETE_PLAYER, new Object[] {id});
        return delete==-1?false:true;
    }
}
