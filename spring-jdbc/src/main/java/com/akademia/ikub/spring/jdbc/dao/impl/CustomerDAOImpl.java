package com.akademia.ikub.spring.jdbc.dao.impl;

import com.akademia.ikub.spring.jdbc.dao.CustomerDAO;
import com.akademia.ikub.spring.jdbc.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

    private static final String LIST_CUSTOMERS_QUERY = "SELECT * FROM CUSTOMERS where DELETED = false";

    private static final String GET_CUSTOMER_BY_ID_QUERY = "SELECT * FROM CUSTOMERS WHERE id = ? and DELETED = false";

    private static final String CREATE_CUSTOMER_QUERY = "INSERT INTO customers (first_name, last_name,email,phone, password, address, zipcode) VALUES " +
            "(?,?,?,?,?,?,?)";

    private static final String UPDATE_CUSTOMER_QUERY = "UPDATE CUSTOMERS SET email = ?, password = ? WHERE id = ?";

    private static final String DELETE_CUSTOMER_QUERY = "UPDATE CUSTOMERS SET deleted = true WHERE id = ?";


    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Customer> getCustomers() {
        return jdbcTemplate
                .query(LIST_CUSTOMERS_QUERY, new BeanPropertyRowMapper<>(Customer.class));
    }

    @Override
    public Customer getCustomerById(Long id) {
        return jdbcTemplate.queryForObject(GET_CUSTOMER_BY_ID_QUERY,new BeanPropertyRowMapper<>(Customer.class),
                new Object[]{id});
    }

    @Override
    public Boolean createCustomer(Customer c) {
        Integer insert =  jdbcTemplate.update(CREATE_CUSTOMER_QUERY,new Object[]{c.getFirstName(),c.getLastName(),c.getEmail(),
        c.getPhone(),c.getPassword(),c.getAddress(),c.getZipcode()});
        return insert==-1?false:true;
    }

    @Override
    public Boolean updateCustomer(Long id, Customer c) {
        Integer update =  jdbcTemplate.update(UPDATE_CUSTOMER_QUERY,
                new Object[]{c.getEmail(),c.getPassword(),id});
        return update==-1?false:true;
    }

    @Override
    public Boolean deleteCustomer(Long id) {
        Integer delete =  jdbcTemplate.update(DELETE_CUSTOMER_QUERY,new Object[]{id});
        return delete==-1?false:true;
    }
}
