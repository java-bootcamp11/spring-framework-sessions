package com.akademia.ikub.spring.jdbc.service;

import com.akademia.ikub.spring.jdbc.model.Order;

import java.util.List;

public interface OrderService {
    List<Order> getOrdersByCustomerId(Long customerId);
}
