package com.akademia.ikub.spring.jdbc.dao.impl;

import com.akademia.ikub.spring.jdbc.dao.OrderDAO;
import com.akademia.ikub.spring.jdbc.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OrderIDAOImpl implements OrderDAO {

    private static final String GET_ORDER_CUSTOMER = "SELECT * FROM ORDERS WHERE customer_id = ?";
    private static final String GET_ORDER_BY_ID = "SELECT * FROM ORDER WHERE id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Order> getOrdersByCustomerId(Long customerId) {
        return jdbcTemplate.query(GET_ORDER_CUSTOMER, new BeanPropertyRowMapper<>(Order.class)
              ,new Object[]{customerId});
    }

    @Override
    public Order getOrderById(Long orderId) {
        return jdbcTemplate.queryForObject(GET_ORDER_BY_ID
                , new BeanPropertyRowMapper<>(Order.class),new Object[]{orderId});
    }
}
