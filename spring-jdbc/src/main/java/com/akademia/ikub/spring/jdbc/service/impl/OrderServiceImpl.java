package com.akademia.ikub.spring.jdbc.service.impl;

import com.akademia.ikub.spring.jdbc.dao.OrderDAO;
import com.akademia.ikub.spring.jdbc.dao.OrderItemsDAO;
import com.akademia.ikub.spring.jdbc.model.Order;
import com.akademia.ikub.spring.jdbc.model.OrderItem;
import com.akademia.ikub.spring.jdbc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDAO orderDAO;

    @Autowired
    private OrderItemsDAO orderItemsDAO;

    @Override
    public List<Order> getOrdersByCustomerId(Long customerId) {
        List<Order> orders = orderDAO.getOrdersByCustomerId(customerId)
                .stream()
                .map(o -> {
                    List<OrderItem> items = orderItemsDAO.getOrderItemsByOrderId(o.getId());
                    o.setOrderItems(items);
                    return o;
                }).collect(Collectors.toList());
        return orders;
    }
}
