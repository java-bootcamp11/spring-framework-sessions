package com.akademia.ikub.spring.jdbc.model;

import java.time.LocalDateTime;
import java.util.List;

public class Order {

  private Long id;

  private Customer customer;

  private Long totalAmount;

  private LocalDateTime orderDate;

  private List<OrderItem> orderItems;

  public Order() {
  }

  public Order(Long id, Customer customer, Long totalAmount, LocalDateTime orderDate, List<OrderItem> orderItems) {
    this.id = id;
    this.customer = customer;
    this.totalAmount = totalAmount;
    this.orderDate = orderDate;
    this.orderItems = orderItems;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer customer) {
    this.customer = customer;
  }

  public Long getTotalAmount() {
    return totalAmount;
  }

  public void setTotalAmount(Long totalAmount) {
    this.totalAmount = totalAmount;
  }

  public LocalDateTime getOrderDate() {
    return orderDate;
  }

  public void setOrderDate(LocalDateTime orderDate) {
    this.orderDate = orderDate;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public void setOrderItems(List<OrderItem> orderItems) {
    this.orderItems = orderItems;
  }

  @Override
  public String toString() {
    return "Order{" +
            "id=" + id +
            ", customer=" + customer +
            ", totalAmount=" + totalAmount +
            ", orderDate=" + orderDate +
            ", orderItems=" + orderItems +
            '}';
  }
}