package com.akademia.ikub.spring.jdbc.dao;

import com.akademia.ikub.spring.jdbc.model.OrderItem;

import java.util.List;

public interface OrderItemsDAO {

    List<OrderItem> getOrderItemsByOrderId(Long orderId);
}
