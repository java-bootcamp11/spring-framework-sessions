package com.akademia.ikub.jpa.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "registration")
public class Registration {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "player_id",referencedColumnName = "id")
    private Player player;

    @ManyToOne
    @JoinColumn(name = "tournament_id",referencedColumnName = "id")
    private Tournament tournament;

    @Override
    public String toString() {
        return "Registration{" +
                "id=" + id +
                '}';
    }
}
