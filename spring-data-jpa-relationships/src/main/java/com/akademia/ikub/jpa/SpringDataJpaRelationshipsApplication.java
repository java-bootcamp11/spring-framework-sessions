package com.akademia.ikub.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class SpringDataJpaRelationshipsApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringDataJpaRelationshipsApplication.class, args);
    }

}
